#!/bin/sh
echo "###############################################################################"
echo "# Blogdown checks"
echo "###############################################################################"
echo ""

echo "***** index.xxx file missing in post/"
echo "*****"
# echo "find ./content/post/ -maxdepth 1 -type d '!' -exec sh -c 'ls -1 "{}"|egrep -i -q "^index\.(md|Rmd|Rmarkdown)$"' ';' -print"
find ./content/post/ -maxdepth 1 -type d '!' -exec sh -c 'ls -1 "{}"|egrep -i -q "^index\.(md|Rmd|Rmarkdown)$"' ';' -print
echo ""
echo ""
echo ""

echo "***** cite.bib missing in post/ or publication/"
echo "*****"
# echo "find ./content/post/ -maxdepth 1 -type d \! -exec test -e '{}/cite.bib' \; -print"
find ./content/post/ -maxdepth 1 -type d \! -exec test -e '{}/cite.bib' \; -print
echo ""
echo "*****"
# echo "find ./content/publication/ -maxdepth 1 -type d \! -exec test -e '{}/cite.bib' \; -print"
find ./content/publication/ -maxdepth 1 -type d \! -exec test -e '{}/cite.bib' \; -print
echo ""
echo ""
echo ""

echo "***** featured.xxx image missing in post/ project/ or publication/"
echo "*****"
# echo "find ./content/post/ -maxdepth 1 -type d '!' -exec sh -c 'ls -1 "{}"|egrep -i -q "^featured\.(jpg|jpeg|png|gif)$"' ';' -print"
find ./content/post/ -maxdepth 1 -type d '!' -exec sh -c 'ls -1 "{}"|egrep -i -q "^featured\.(jpg|jpeg|png|gif)$"' ';' -print
echo ""
echo "*****"
# echo "find ./content/project/ -maxdepth 1 -type d '!' -exec sh -c 'ls -1 "{}"|egrep -i -q "^featured\.(jpg|jpeg|png|gif)$"' ';' -print"
find ./content/project/ -maxdepth 1 -type d '!' -exec sh -c 'ls -1 "{}"|egrep -i -q "^featured\.(jpg|jpeg|png|gif)$"' ';' -print
echo ""
echo "*****"
# echo "find ./content/publication/ -maxdepth 1 -type d '!' -exec sh -c 'ls -1 "{}"|egrep -i -q "^featured\.(jpg|jpeg|png|gif)$"' ';' -print"
find ./content/publication/ -maxdepth 1 -type d '!' -exec sh -c 'ls -1 "{}"|egrep -i -q "^featured\.(jpg|jpeg|png|gif)$"' ';' -print