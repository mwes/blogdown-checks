In writing with `R` package `blogdown` that uses `hugo`, I worried I would forget things.
E.g., adding an image or citation, or making an image visible

This script checks your site folder for these problems.